import requests
import re
from bs4 import BeautifulSoup as BS
import datetime
from io import BytesIO
from PyPDF2 import PdfFileReader, PdfFileWriter


def get_gis_page(url='https://gisbarbados.gov.bb/covid-19/'):
    html = requests.get(url).text
    return html

def get_all_links(html):
    soup = BS(html)
    links = soup.find_all('a', href=True)
    return links

def get_last_covid_update():
    html = get_gis_page()
    links = get_all_links(html)

    covid_links = list(set([link.parent.parent for link in links if "covid-19-update" in link['href']]))
    last_link = covid_links.pop()
    for link in covid_links:
        last_date_text = last_link.findAll('span', {'class': 'published'})[0].text
        link_date_text = link.findAll('span', {'class': 'published'})[0].text
        if datetime.datetime.strptime(link_date_text, "%B %d, %Y").date() > datetime.datetime.strptime(last_date_text, "%B %d, %Y").date():
            last_link = link
    return last_link

# link.find_all('a', href=True).pop(1)['href']

def get_update_doc(url):
    html = get_gis_page(url=url)
    links = get_all_links(html)
    pdf_url = [link['href'] for link in links if ".pdf" in link['href']].pop()
    pdf_html = requests.get(pdf_url)
    pdf_file = BytesIO(pdf_html.content)
    existing_pdf = PdfFileReader(pdf_file)
    pdf_writer = PdfFileWriter()
    for page in range(existing_pdf.getNumPages()):
        pdf_writer.addPage(existing_pdf.getPage(page))
    return pdf_writer